Frederick Trisjono <ftrisjono@yahoo.com> \| (408) 601-9036 \| P.O. BOX 20201, SAN JOSE, CA 95160

Professional Experience

[[Nvidia Corp. \~ Santa Clara, CA]{.smallcaps} 2005-Present]{.underline}

**Sr. System Integration Manager (2012-Present)**

Currently working on end to end development of deep learning optical inspection software system. Established a system test engineering team for GPU, Shield, and server product lines. Work with clients on strategy development for improving manufacturing DPPM:

-   Improved product/testing quality by launching task force to drive cross-functional solutions, resulting in implementation of ATE and System product-test, ASIC verification, and new designs.

-   Led development of and served as primary architect for failure-investigation software that diagnoses malfunction at board level, collects data, and offers repair guide solutions.

-   Optimized test coverage and reduced cost by incorporating customer feedback into integration of internal system (functional tests) and chip level (CP and FT of ATE) verification.

**Mobile Test Engineering Manager (2007-2012)**

Retained to build and support Tegra chip testing and was promoted to Manager. Capitalized on previous GPU experience to lead transition to mobile and automotive industries. Directed various test project definition exercises with cross-functional teams including design, architecture, and marketing. Expanded team to 19 employees.

-   Executed "speed-of-light" production test program developments with teams in Taiwan, China, India, and the UK.

-   Implemented new methods to improve processes for managing hardware and software, including EPROM programming for hardware ID, binning simulator for verification, and system testing on ATE.

-   Introduced rotating roles within team to relieve new-product development burdens and sharpen members' skills.

-   Initiated use of industry best practices in infrastructure and baseline reference for new product development and testing, including application model, HDCP infrastructure, and functional testing.

-   Led development of automatic and scalable test-program generator and security test method software.

**Staff Test / Product Engineer (2005 -- 2007)**

-   Selected as product and test lead for company's first automotive product, creating automotive-level requirements in product qualification, testing, and reporting; recognized internally for various process improvements and findings, including power ramp-up programming for safe testing and automation of characterization.

Education\
[Master of Science in computer engineering]{.underline} [[(San Jose State University)]{.smallcaps}]{.underline}

-   Client-Server Computing & Computer Architecture; Project: MedFlex, a Medical App for Hand-Held Computers

[Bachelor of science in electrical engineering [(university of utah)]{.smallcaps}]{.underline}

-   "Interactive Reconstruction of Fluorine-18 SPECT Using Geometric Response Correction." *Journal of Nuclear Medicine*, Vol. 39(1) 1998. Pp 124-130.

Career Development\
[[Cypress Semiconductor \~ San Jose, CA (2003-2005)]{.underline}]{.smallcaps}\
**Test Automation Project Lead (2003-2005)**

-   Led and developed code for web-based software that generates a test program automatically, shaving time-to-market and reducing costs for various test development and management.

**Senior Product Engineer for SRAM** **products (1996-2000)**

-   SRAM products bring up and process improvement and other key projects, including class test at sort (Known Good Die), methods to test voltage regulators, testing scalability, and development using quick-build packaging; received internal company awards.

[[VMWare \~ Palo Alto, CA (2002-2003)]{.underline}]{.smallcaps}

**Software Engineer**

-   Individual contributor on core-device virtualization team for keyboard interface, APIC, RTC, CPU, and virtualized BIOS.

[[Transmeta Corp. \~ Santa Clara, CA (2002)]{.underline}]{.smallcaps}

**Software Engineer**

-   Key member of software development and device modeling teams, creating and maintaining pieces of AGP, PCI, and RTC software device models and work on device virtualization used to verify Transmeta's microprocessor.

-   Maintained engineering BIOS customization for the device model: Anna BIOS and proto board.

Additional Information

**Technical skills**

[Operating Platforms]{.underline}: Windows, Linux, UNIX, Docker, VMs \| [Languages]{.underline}: Python, C, C++, Perl, Shell Scripting, Java, Angular, HTML, CGI, Javascript, SQL \| [Automatic Testing Equipment]{.underline}: Advantest V93000 & T2000, Teradyne iFlex, Oscilloscopes, TDR
