pandoc -s .\TrisjonoFred.docx --wrap=none --reference-links -t markdown -o resume.md

# command to diff WORDS docx with git
git wdiff file.docx

# command to see WORD docx changes over time
git log -p --word-diff=color file.docx
